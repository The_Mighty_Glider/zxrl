#include <ucase.bas>

dim runing as Byte

dim c as Byte

dim dice as uByte
dim coin as Byte
dim key as uByte

dim x as Byte
dim y as Byte

dim xx as Byte
dim yy as Byte

dim PlayerX as Byte
dim PlayerY as Byte
dim PlayerPower as Byte
dim PlayerHealth as Byte
dim PlayerHealthMax as Byte
dim PlayerSanity as Byte
dim PlayerRations as uByte
dim PlayerMeat as uByte
dim PlayerHealingPotion as Byte
dim PlayerSanityPotion as Byte
dim PlayerSymbol as uByte
dim PlayerRing as uByte
dim PlayerPoison as Byte
dim PlayerCurse as Byte
dim PlayerTeleport as Byte
dim PlayerHolyLight as Byte

dim DungeonLevel as Byte
dim DungeonSymbol as Byte
dim DungeonRing as Byte

dim message1 as string
dim message2 as string
dim message3 as string

sub initPlayer()
  PlayerPower = 9
  PlayerHealth = 20
  PlayerHealthMax = 20
  PlayerSanity = 99
  PlayerRations = 3
  PlayerMeat = 0
  PlayerHealingPotion = 0
  PlayerSanityPotion = 0
  DungeonLevel = 1
  DungeonSymbol = (rnd*5) + 3
  DungeonRing = (rnd*3) + 5
  PlayerSymbol = 0
  PlayerRing = 0
  PlayerPoison = 0
  PlayerCurse = 0
  message1 = " "
  message2 = " "
  message3 = " "
  coin = rnd*2
  if coin = 0 then
    PlayerTeleport = 1
    PlayerHolyLight = 0
  else
    PlayerTeleport = 0
    PlayerHolyLight = 1
  end if

end sub

dim DungeonMap(19,19) as Byte
dim ExplorationMap(3,3) as Byte

dim roomz (8,4,4) as Byte => {_
{_
{0,0,1,0,0},_
{0,1,1,1,0},_
{1,1,1,1,1},_
{0,1,1,1,0},_
{0,0,1,0,0}},_
{_
{0,0,1,0,0},_
{0,0,1,0,0},_
{1,1,1,1,1},_
{0,0,1,0,0},_
{0,0,1,0,0}},_
{_
{0,0,1,0,0},_
{0,1,1,1,0},_
{1,1,2,1,1},_
{0,1,1,1,0},_
{0,0,1,0,0}},_
{_
{3,3,1,3,3},_
{3,3,1,3,3},_
{1,1,1,1,1},_
{3,3,1,3,3},_
{3,3,1,3,3}},_
{_
{0,1,1,1,0},_
{1,1,1,1,1},_
{1,1,1,1,1},_
{1,1,1,1,1},_
{0,1,1,1,0}},_
{_
{1,2,1,2,1},_
{1,1,1,1,1},_
{1,2,1,2,1},_
{1,1,1,1,1},_
{1,2,1,2,1}},_
{_
{1,1,1,1,1},_
{1,0,0,0,1},_
{1,0,0,0,1},_
{1,0,0,0,1},_
{1,1,1,1,1}},_
{_
{1,1,1,1,1},_
{1,3,3,3,1},_
{1,3,3,3,1},_
{1,3,3,3,1},_
{1,1,1,1,1}},_
{_
{2,2,2,2,2},_
{2,1,1,1,2},_
{1,1,-66,1,2},_
{2,1,1,1,2},_
{2,2,2,2,2}}_
}

dim MonsterList (12) as Byte => {_
-77,_
-62,_
-59,_
-54,_
-61,_
-67,_
-57,_
-73,_
-68,_
-60,_
-78,_
-72,_
-66}

dim NameList (12) as String

let NameList(0) = "cockroach"
let NameList(1) = "rat"
let NameList(2) = "ugly worm"
let NameList(3) = "zombie"
let NameList(4) = "skeleton"
let NameList(5) = "mummy"
let NameList(6) = "wiedergaenger"
let NameList(7) = "ghoul"
let NameList(8) = "lich"
let NameList(9) = "thing"
let NameList(10) = "bone monster"
let NameList(11) = "eldritch horror"
let NameList(12) = "necromancer"

dim AbilityList(12) as Byte => {_
0,_
0,_
0,_
0,_
0,_
2,_
0,_
1,_
2,_
1,_
0,_
2,_
3}

dim SanityList (12) as uByte => {_
0,_
0,_
0,_
1,_
2,_
4,_
5,_
5,_
7,_
7,_
7,_
10,_
0}

dim MonsterPower as uByte
dim MonsterSanityEffect as uByte
dim MonsterName as string 
dim MonsterAbility as Byte

MonsterName = "n/a"
MonsterPower = 7
MonsterSanityEffect = 0
MonsterAbility = 0

dim ObjectName(15) as String

let ObjectName(0) = "Wall"
let ObjectName(1) = "Floor"
let ObjectName(2) = "Pilar"
let ObjectName(3) = "Water"
let ObjectName(4) = "Orb of Power"
let ObjectName(5) = "Stair"
let ObjectName(6) = "Ration"
let ObjectName(7) = "Foul Meat"
let ObjectName(8) = "Altar (unused)"
let ObjectName(9) = "Altar (used)"
let ObjectName(10) = "Healing Potion"
let ObjectName(11) = "Sanity Potion"
let ObjectName(12) = "Holy Symbol"
let ObjectName(13) = "Ring of Poison Resist."
let ObjectName(14) = "Scroll of Teleportation"
let ObjectName(15) = "Scroll of Holy Light"

sub rollDice
  dice = (rnd*6)+1
  if dice = 1 or dice = 3 or dice = 5 then
    dice = -1*dice
  end if

end sub

sub setUDC()
  dim char (15,7) as uByte => { {_
  %10111011,_
  %10111011,_
  %10111011,_
  %00000000,_
  %11101110,_
  %11101110,_
  %11101110,_
  %00000000},_
  {_
  %01010101,_
  %10101010,_
  %01010101,_
  %10101010,_
  %01010101,_
  %10101010,_
  %01010101,_
  %10101010},_
  {_
  %01111110,_
  %00100100,_
  %00011000,_
  %00011000,_
  %00011000,_
  %00011000,_
  %00100100,_
  %01111110},_
  {_
  %00000000,_
  %00110011,_
  %11001100,_
  %00000000,_
  %00000000,_
  %00110011,_
  %11001100,_
  %00000000},_
  {_
  %00000000,_
  %00111100,_
  %01101110,_
  %01011110,_
  %01011110,_
  %01101110,_
  %00111100,_
  %00000000},_
  {_
  %00000011,_
  %00000011,_
  %00001111,_
  %00001111,_
  %00111111,_
  %00111111,_
  %11111111,_
  %11111111},_
  {_
  %00000000,_
  %01100110,_
  %10011001,_
  %01111110,_
  %01111110,_
  %10011001,_
  %01100110,_
  %00000000},_
  {_
  %01000000,_
  %11000000,_
  %00100100,_
  %00111110,_
  %01111100,_
  %00100100,_
  %00000011,_
  %00000010},_
  {_
  %00000000,_
  %00000000,_
  %00000000,_
  %01000010,_
  %00111100,_
  %01111110,_
  %01000010,_
  %01111110},_
  {_
  %00011000,_
  %01100110,_
  %00011000,_
  %01000010,_
  %00111100,_
  %01111110,_
  %01000010,_
  %01111110},_
  {_
  %00011000,_
  %00111100,_
  %00011000,_
  %00101100,_
  %01011110,_
  %01011110,_
  %01011110,_
  %00111100},_
  {_
  %00011000,_
  %00111100,_
  %00011000,_
  %01111110,_
  %01000110,_
  %01011110,_
  %01011110,_
  %01111110},_
  {_
  %00111100,_
  %00100100,_
  %11100111,_
  %10000001,_
  %10000001,_
  %11100111,_
  %00100100,_
  %00111100},_
  {_
  %00000000,_
  %00111100,_
  %01100110,_
  %01000010,_
  %01000010,_
  %01100110,_
  %00111100,_
  %00000000},_
  {_
  %00000000,_
  %01111110,_
  %00100011,_
  %00110111,_
  %01101110,_
  %11111100,_
  %11111100,_
  %00000000},_
  {_
  %00000000,_
  %01111110,_
  %00101011,_
  %00100011,_
  %01010110,_
  %11111100,_
  %11111100,_
  %00000000}_
  }

  for x = 0 to 15
    for y = 0 to 7
      poke usr "A" + y + x * 8,char(x,y)
    next
  next

end sub

sub setColor()
  paper 0
  ink 7
  border 0

end sub

sub addMessage(mes as string)
  ink 0
  print at 23,0;message1
  print at 22,0;message2
  print at 21,0;message3
  message3 = message2
  message2 = message1
  message1 = mes
  ink 7
  bright 1
  print at 23,0;message1
  bright 0
  print at 22,0;message2
  print at 21,0;message3
  bright 1

end sub

sub drawSidebar()
  ink 7
  bright 1
  print at 0,22;"Crypt"
  print at 0,28;DungeonLevel
  if PlayerRing = 0 then
    ink 2
  else
    ink 4
    bright 0
  end if
  print at 3,23;"Health"
  print at 4,24;"  "
  print at 4,24;PlayerHealth
  print at 4,26;"/"
  bright 1
  print at 4,27;PlayerHealthMax
  if PlayerSymbol = 0 then
    ink 1
  else
    ink 6
  end if
  print at 7,23;"Sanity"
  print at 8,24;"  "
  print at 8,24;PlayerSanity
  print at 8,26;"%"
  if PlayerCurse > 0 and PlayerSymbol = 0 then
    ink 5
    print at 12,21;"Cursed(" + str PlayerCurse + ")"
  else
    print at 12,21;"          "
  end if
  if PlayerPoison > 0 and PlayerRing = 0 then
    ink 4
    print at 13,20;"Poisoned(" + str PlayerPoison + ")"
  else
    print at 13,20;"           "
  end if
  ink 4
  bright 0
  print at 16,23;chr$(157) + "x" + str(PlayerRing)
  bright 1
  ink 6
  print at 16,27;chr$(156) + "x" + str(PlayerSymbol)
  ink 2
  print at 17,23;chr$(154) + "x" + str(PlayerHealingPotion)
  ink 1
  print at 17,27;chr$(155) + "x" + str(PlayerSanityPotion)
  ink 3
  print at 18,23;chr$(150) + "x" + str(PlayerRations)
  ink 5
  print at 18,27;chr$(151) + "x" + str(PlayerMeat)
  print at 19,23;chr$(158) + "x" + str(PlayerTeleport)
  ink 6
  print at 19,27;chr$(159) + "x" + str(PlayerHolyLight)

end sub

sub drink()
  paper 7
  ink 1
  print at 21,0;"    Drink what? (C to leave)    "
  paper 1
  ink 7
  print at 22,0;"    A - Healing Potion(" + str(PlayerHealingPotion) + "x)      "
  print at 23,0;"    B - Sanity  Potion(" + str(PlayerSanityPotion) + "x)      "
  paper 0
  ink 1
  c = 1
  do
    waitKey()
    if key = code "a" then
      c = 0
      clearMessages()
      if PlayerHealingPotion > 0 then
        addMessage("You drink a healing potion.")
        PlayerHealingPotion = PlayerHealingPotion - 1
        PlayerHealth = PlayerHealth + 9
        if PlayerHealth > PlayerHealthMax then
          PlayerHealth = PlayerHealthMax
        end if
        drawSidebar()
      else
        addMessage("You don't own this potion.")
      end if
    elseif key = code "b"
      c = 0
      clearMessages()
      if PlayerSanityPotion > 0 then
        addMessage("You drink a sanity potion.")
        PlayerSanityPotion = PlayerSanityPotion - 1
        PlayerSanity = PlayerSanity + 20
        if PlayerSanity > 99 then
          PlayerSanity = 99
        end if
        drawSidebar()
      else
        addMessage("You don't own this potion.")
      end if
    elseif key = code "c"
      c = 0
      clearMessages()
      addMessage("Nevermind")
    end if
  loop while c = 1

end sub

sub readScroll()
  paper 7
  ink 3
  print at 21,0;"    Read what? (C to leave)     "
  paper 3
  ink 7
  print at 22,0;"   A - Scroll of Teleport(" + str(PlayerTeleport) + "x)   "
  print at 23,0;"  B - Scroll of Holy Light(" + str(PlayerHolyLight) + "x)  "
  paper 0
  ink 1
  c = 1
  do
    waitKey()
    if key = code "a" then
      c = 0
      clearMessages()
      if PlayerTeleport > 0 then
        addMessage("You read the scroll.")
        addMessage("Everything blurs for a moment.")
        addMessage("The scroll crumbles to dust.")
        PlayerTeleport = PlayerTeleport - 1
        findLocation()
        xx = PlayerX
        yy = PlayerY
        PlayerX = x
        PlayerY = y
        drawMap(xx-4,xx+4,yy-4,yy+4)
        drawSidebar()
      else
        addMessage("You don't own this scroll.")
      end if
    elseif key = code "b"
      c = 0
      clearMessages()
      if PlayerHolyLight > 0 then
        addMessage("You read the scroll.")
        addMessage("The holy light shines bright.")
        addMessage("The scroll crumbles to dust.")
        PlayerHolyLight = PlayerHolyLight - 1
        for x = PlayerX-1 to PlayerX+1
          for y = PlayerY-1 to PlayerY+1
            if DungeonMap(x,y) < 0 and not DungeonMap(x,y) = -66 then
              let DungeonMap(x,y) = 1
            end if
          next
        next
        drawSidebar()
      else
        addMessage("You don't own ths scroll.")
      end if
    elseif key = code "c"
      c = 0
      clearMessages()
      addMessage("Nevermind")
    end if
  loop while c = 1

end sub

sub examine()
  dim ex as uByte
  dim ey as uByte
  dim r as uByte
  ex = PlayerX
  ey = PlayerY
  r = 1
  do
    drawMap(PlayerX-4,PlayerX+4,PlayerY-4,PlayerY+4)
    paper 0
    ink 5
    print at ex,ey;"X"
    clearMessages()
    paper 5
    ink 0
    print at 21,0;"       Examine (C to leave)     "
    paper 0
    ink 5
    if DungeonMap(ex,ey) < 0 then
      setMonsterStats(DungeonMap(ex,ey))
      print at 22,0;"Monster: " + UCase(MonsterName)
      print at 23,0;"Power: " + str(MonsterPower)
    else
      print at 22,0;ObjectName(DungeonMap(ex,ey))
    end if
    waitKey()
    if key = code "w" then
      if ex > PlayerX - 2 and ex > 0 then
        ex = ex - 1
      end if
    elseif key = code "s"
      if ex < PlayerX + 2 and ex < 19 then
        ex = ex + 1
      end if
    elseif key = code "a"
      if ey > PlayerY - 2 and ey > 0 then
        ey = ey - 1
      end if
    elseif key = code "d"
      if ey < PlayerY + 2 and ey < 19 then
        ey = ey + 1
      end if
    elseif key = code "c"
      r = 0
      clearMessages()
      addMessage("Nevermind")
    else
      r = 1
    end if
  loop while r = 1

end sub

sub eat()
  paper 7
  ink 2
  print at 21,0;"     Eat what? (C to leave)     "
  paper 2
  ink 7
  print at 22,0;"         A - Ration(" + str(PlayerRations) + "x)         "
  print at 23,0;"       B - Foul Meat(" + str(PlayerMeat) + "x)        "
  paper 0
  ink 1
  c = 1
  do
    waitKey()
    if key = code "a" then
      c = 0
      clearMessages()
      if PlayerRations > 0 then
        addMessage("You eat a ration.")
        PlayerRations = PlayerRations - 1
        PlayerHealth = PlayerHealth + 7
        if PlayerHealth > PlayerHealthMax then
          PlayerHealth = PlayerHealthMax
        end if
        PlayerSanity = PlayerSanity + 7
        if PlayerSanity > 99 then
          PlayerSanity = 99
        end if
        drawSidebar()
      else
        addMessage("You don't own rations.")
      end if
    elseif key = code "b"
      c = 0
      clearMessages()
      if PlayerMeat > 0 then
        if PlayerSanity > 79 then
         addMessage("You would never eat this!")
        else
          PlayerMeat = PlayerMeat - 1
          addMessage("You eat the foul meat.")
          if PlayerSanity > 49 then
            addMessage("It is terrible!")
          else
            addMessage("It is tasty!")
          end if
          if PlayerSanity > 30 then
            PlayerSanity = PlayerSanity - 2
            if PlayerSanity < 30 then
              PlayerSanity = 30
            end if
          end if
          PlayerHealth = PlayerHealth + 7
          if PlayerHealth > PlayerHealthMax then
            PlayerHealth = PlayerHealthMax
          end if
          drawSidebar()
        end if
      else
        addMessage("You don't own foul meat.")
      end if
    elseif key = code "c"
      c = 0
      clearMessages()
      addMessage("Nevermind")
    end if
  loop while c = 1

end sub

sub clearMessages()
  dim freeSpace as string
  freeSpace = "                                "
  paper 0
  ink 7
  for x = 0 to 2
    print at 21+x,0;freeSpace
  next

end sub
sub resetExploration()
  for x = 0 to 3
    for y = 0 to 3
      let ExplorationMap(x,y) = 0
    next
  next

end sub

sub updateExploration
  x =  PlayerX/5
  y =  PlayerY/5
  let ExplorationMap(x,y) = 1
  if x > 0 then
   let ExplorationMap(x-1,y) = 1
  end if
  if x < 3 then
   let ExplorationMap(x+1,y) = 1
  end if
  if y > 0 then
   let ExplorationMap(x,y-1) = 1
  end if
  if y < 3 then
   let ExplorationMap(x,y+1) = 1
  end if

end sub

sub setMonsterStats(n as Byte)
  c = -1
  do
    c = c + 1
  loop while n <> MonsterList(c)
  MonsterPower = 6+c
  MonsterAbility = AbilityList(c)
  MonsterSanityEffect = SanityList(c)
  if PlayerSanity < 16 or PlayerSymbol = 1 then
    MonsterSanityEffect = 1
  end if
  MonsterName = NameList(c)

end sub

sub fightMonster(PlayerAttack as uByte, xPos as Byte, yPos as Byte)
  dim mes as String
  dim p as Byte
  dim m as Byte
  dim damage as Byte
  setMonsterStats(DungeonMap(xPos,yPos))
  rollDice()
  p = PlayerPower + dice
  rollDice()
  m = MonsterPower + dice
  if PlayerAttack = 1 then
    mes = "You attack the " + MonsterName + "."
  else
    mes = "The " + MonsterName + " attacks you."
  end if
  addMessage(mes)
  c = p - m
  if c <> 0 then
    damage = 3 - (c/3)
  else
    damage = 3
  end if 
    if damage < 0 then
      damage = 0
    end if
    PlayerHealth = PlayerHealth - damage
    PlayerSanity = PlayerSanity - MonsterSanityEffect
    drawSidebar()
    mes = "You take " + str damage + "HP damage."
    addMessage(mes)
    if c > (-1) then
      mes = "You kill the " + MonsterName + "."
      addMessage(mes)
      coin = rnd*10
      if coin > 7 then
        let DungeonMap(xPos,yPos) = 7
      else
        let DungeonMap(xPos,yPos) = 1
      end if
      if MonsterName = "necromancer" then
        waitKey()
        runing = 0
        if PlayerSanity > 50 then
          deadScreen(2)
        else
          deadScreen(3)
        end if
      end if
    else
      if MonsterPower > p + 6 then
        mes = "You can't kill it!"
      else
        mes = "The " + MonsterName + " survives."
      end if
      addMessage(mes)
    end if

end sub

sub monsterSummonesMonster(posX as Byte,posY as Byte)
  for x = posX - 1 to posY +1
    for y = posY - 1 to posY + 1
      if DungeonMap(x,y) = 1 and DungeonMap(posX,posY) < 0 then
        coin = 5 + rnd*5
        let DungeonMap(x,y) = MonsterList(coin)
        let DungeonMap(posX,posY) = DungeonMap(posX,posY) * (-1)
        addMessage("Something is summoned!")
      end if
    next
  next

end sub

sub moveMonsters(xStart as Byte,xEnd as Byte,yStart as Byte,yEnd as Byte)
  dim dist as Byte
  dim newDist as Byte
  if xStart < 1 then
    xStart = 1
  end if
  if yStart < 1 then
    yStart = 1
  end if
  if xEnd > 18 then
    xEnd = 18
  end if
  if yEnd > 18 then
    yEnd = 18
  end if
  for x = xStart to xEnd
    for y = yStart to yEnd
      coin = rnd*10
      if coin > 3 then
        if DungeonMap(x,y) < 0 then
          xx = x
          yy = y
          dist = (x-PlayerX)*(x-PlayerX)+(y-PlayerY)*(y-PlayerY)
          setMonsterStats(DungeonMap(x,y))
          if MonsterAbility = 3 then
            coin = rnd*6
            if coin < 4 then
              monsterSummonesMonster(x,y)
            end if
          end if
          if dist = 1 then
            if MonsterAbility = 1 then
               coin = rnd*6
               if coin < 3 and PlayerRing = 0 then
                 PlayerPoison = PlayerPoison + 1 + rnd*6
                 addMessage("The "+MonsterName+" poisons you.")
                 let DungeonMap(x,y) = DungeonMap(x,y) * (-1)
               end if
            elseif MonsterAbility = 2
              coin = rnd*6
              if coin < 3 and PlayerSymbol = 0 then
                PlayerCurse = PlayerCurse + 1 + rnd*6
                addMessage("The "+MonsterName+" curses you.")
                let DungeonMap(x,y) = DungeonMap(x,y) * (-1)
              end if
            end if
            if DungeonMap(x,y) < 0 then
              fightMonster(0,x,y)
            end if
            if DungeonMap(x,y) < 0 then
              let DungeonMap(x,y) = DungeonMap(x,y) * (-1)
            end if
          end if
          if DungeonMap(x,y) < 0 then
            rem check x-1
            newDist = (x-1-PlayerX)*(x-1-PlayerX)+(y-PlayerY)*(y-PlayerY)
            if DungeonMap(x-1,y) = 1 and newDist < dist and newDist <> 0 then
              dist = newDist
              xx = x-1
              yy = y
            end if
            rem check x+1
            newDist = (x+1-PlayerX)*(x+1-PlayerX)+(y-PlayerY)*(y-PlayerY)
            if DungeonMap(x+1,y) = 1 and newDist < dist and newDist <> 0 then
              dist = newDist
              xx = x+1
              yy = y
            end if
            rem check y-1
            newDist = (x-PlayerX)*(x-PlayerX)+(y-1-PlayerY)*(y-1-PlayerY)
            if DungeonMap(x,y-1) = 1 and newDist < dist and newDist <> 0 then
              dist = newDist
              xx = x
              yy = y-1
            end if
            rem chech y+1
            newDist = (x-PlayerX)*(x-PlayerX)+(y+1-PlayerY)*(y+1-PlayerY)
            if DungeonMap(x,y+1) = 1 and newDist < dist and newDist <> 0 then
              xx = x
              yy = y+1
            end if
            if xx <> PlayerX or yy <> PlayerY then
              if xx <> x or yy <> y then
                let DungeonMap(xx,yy) = DungeonMap(x,y)*(-1)
                let DungeonMap(x,y) = 1
              end if
            end if
          end if
        end if
      end if
    next
  next
  for x = xStart to xEnd
    for y = yStart to yEnd
      if DungeonMap(x,y) > 23 then
        let DungeonMap(x,y) = DungeonMap(x,y) * (-1)
      end if
    next
  next

end sub

sub findLocation()
  do
    do
      x = rnd*20
      y = rnd*20
    loop while DungeonMap(x,y) <> 1
  loop while x < 5 and y < 5

end sub

sub dungeonGenerator()
  resetExploration()
  PlayerX = 2
  PlayerY = 2
  updateExploration()
  dim m as Byte

  for x = 0 to 3
    for y = 0 to 3
      coin = int (rnd*8)
      if x = 0 and y = 0 then
        coin = 0
      end if
      if DungeonLevel = 10 then
        if x = 3 and y = 3 then
          coin = 8
        end if
      end if
      for xx = 0 to 4
        for yy = 0 to 4
          let DungeonMap(x*5+xx,y*5+yy) = roomz(coin,xx,yy)
        next
      next
    next
  next
  for x = 0 to 19
    for y = 0 to 19
      if x = 0 then
        let DungeonMap(x,y) = 0
      elseif x = 19
        let DungeonMap(x,y) = 0
      end if
      if y = 0 then
        let DungeonMap(x,y) = 0
      elseif y = 19
        let DungeonMap(x,y) = 0
      end if
    next
  next
  rem set orb of power
  findLocation()
  let DungeonMap(x,y) = 4
  rem stair down
  if DungeonLevel < 10 then
    findLocation()
    let DungeonMap(x,y) = 5
  end if
  rem ration
  findLocation()
  let DungeonMap(x,y) = 6
  rem set altar
  if DungeonLevel = 3 or DungeonLevel = 6 then
    findLocation()
    DungeonMap(x,y) = 8
    addMessage("You feel a weak holy vibe.")
  end if
  rem set potions
  coin = rnd*3
  if coin = 0 then
    findLocation()
    DungeonMap(x,y) = 10
  elseif coin = 1
    findLocation()
    DungeonMap(x,y) = 11
  end if
  rem set holy symbol
  if DungeonLevel = DungeonSymbol then
    findLocation()
    let DungeonMap(x,y) = 12
  end if
  rem set ring of poison res.
  if DungeonLevel = DungeonRing then
    findLocation()
    let DungeonMap(x,y) = 13
  end if
  rem set scrolls
    coin = rnd*4
    if coin = 0 then
      findLocation()
      let DungeonMap(x,y) = 14
    elseif coin = 1
      findLocation()
      let DungeonMap(x,y) = 15
    end if
  rem set monsters
  for c = 0 to 10 + DungeonLevel
    findLocation()
    coin = rnd*3
    m = MonsterList(DungeonLevel + coin - 1)
    let DungeonMap(x,y) = m
  next
end sub

sub drawMap(startX as Byte, rangeX as Byte,startY as Byte,rangeY as Byte)
  dim charNum as uByte
  dim char as string
  dim px as uByte
  dim py as uByte
  if startX < 0 then
    startX = 0
  end if
  if rangeX > 19 then
    rangeX = 19
  end if
  if startY < 0 then
    startY = 0
  end if
  if rangeY > 19 then
    rangeY = 19
  end if
  for x = startX to rangeX
    for y = startY to rangeY
      xx = x/5
      yy =y/5
      charNum = DungeonMap(x,y) + 144
      char = chr$ charNum
      if x > PlayerX-3 and x < PlayerX+3 and y > PlayerY-3 and y < PlayerY+3 then
	if charNum =144 then
	  ink 4
	 elseif charNum = 146
	   ink 4
           bright 0
	 elseif charNum = 147
           ink 5
         elseif charNum = 148
           ink 5
         elseif charNum = 149
           ink 7
           bright 0
         elseif charNum = 150
           ink 3
         elseif charNum = 151
           ink 5
         elseif charNum = 152
           ink 5
         elseif charNum = 153
           ink 6
         elseif charNum = 154
           ink 2
         elseif charNum = 155
           ink 1
         elseif charNum = 156
           ink 6
         elseif charNum = 157
           ink 4
           bright 0
         elseif charNum = 158
           ink 5
         elseif charNum = 159
           ink 6
         elseif charNum = 67
           ink 6
         elseif charNum = 82
           ink 2
         elseif charNum = 85
           ink 5
         elseif charNum = 90
           ink 4
         elseif charNum = 83
           ink 7
         elseif charNum = 77
           ink 6
         elseif charNum = 87
           ink 3
         elseif charNum = 71
           ink 2
         elseif charNum = 76
           ink 1
         elseif charNum = 84
           ink 3
         elseif charNum = 66
           ink 7
         elseif charNum = 72
           ink 4
         elseif charNum = 78
           ink 6
         else
           ink 7
        end if
      else
        ink 1
        if charNum < 144 then
          char = chr$ 145
        end if
      end if
      if ExplorationMap(xx,yy) = 0 then
        ink 0
      end if
      
      if x = PlayerX and y = PlayerY then
        char = "@"
        ink 7
      end if
      
      print at x , y ; char
      bright 1
    next
  next
   
end sub

sub waitKey()
  poke 23560,0
  do
    rem empty loop
  loop while peek 23560 = 0
  key = peek 23560
  
end sub

sub userInput()
  waitKey()
  if key = code "a" then
    movePlayer(0,-1)
  elseif key = code "d"
    movePlayer(0,1)
  elseif key = code "s"
    movePlayer(1,0)
  elseif key = code "w"
    movePlayer(-1,0)
  elseif key = code "p"
    movePlayer(0,0)
  elseif key = code "e"
    eat()
  elseif key = code "q"
    drink()
  elseif key = code "r"
    readScroll()
  elseif key = code "x"
    examine()
  end if
  
end sub

sub movePlayer(xDir as Byte,yDir as Byte)
  if PlayerPoison > 0 then
    PlayerPoison = PlayerPoison - 1
    if PlayerRing = 0 then
      PlayerHealth = PlayerHealth - 1
      drawSidebar()
      checkDead()
    end if
  end if
  if PlayerCurse > 0 then
    PlayerCurse = PlayerCurse - 1
    if PlayerSymbol = 0 then
      PlayerSanity = PlayerSanity - 1
      drawSidebar()
      checkDead()
    end if
  end if
  if xDir = 0 and yDir = 0 then
    addMessage("You rest for a moment.")
    return
  end if
  if DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1 then
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 4
    addMessage("You find an orb of power.")
    addMessage("Suddenly you feel stronger!")
    PlayerPower = PlayerPower + 1
    PlayerHealth = PlayerHealth + 1
    PlayerHealthMax = PlayerHealthMax + 1
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
    DungeonMap(PlayerX,PlayerY) = 1
    drawSidebar()
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 5
    DungeonLevel = DungeonLevel + 1
    dungeonGenerator()
    cls
    drawSidebar()
    drawMap(PlayerX-5,PlayerX+5,PlayerY-5,PlayerY+5)
    addMessage("You reach a deeper level.")
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 6
    addMessage("You find a ration.")
    if PlayerRations < 9 then
      PlayerRations = PlayerRations + 1
      let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
      drawSidebar()
    else
      addMessage("You can't carry any more.")
    end if
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 7
    addMessage("You find foul meat.")
    if PlayerMeat < 9 then
      PlayerMeat = PlayerMeat + 1
      let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
      drawSidebar()
    else
      addMessage("You can't carry any more.")
    end if
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 8
    DungeonMap(PlayerX+xDir,PlayerY+yDir) = 9
    addMessage("You pray to the gods of light.")
    addMessage("The gods are pleased!")
    if PlayerHealth < 15 then
      PlayerHealth = 15
    end if
    if PlayerSanity < 75 then
      PlayerSanity = 75
    end if
    drawSidebar()
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 10
    addMessage("You find a healing potion.")
    let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
    PlayerHealingPotion = PlayerHealingPotion + 1
    drawSidebar()
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 11
    addMessage("You find a sanity potion.")
    let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
    PlayerSanityPotion = PlayerSanityPotion + 1
    drawSidebar()
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 12
    addMessage("You find a holy symbol.")
    PlayerSymbol = 1
    let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
    drawSidebar()
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 13
    addMessage("You find a ring of pois. res.")
    PlayerRing = 1
    let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
    drawSidebar()
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 14
    addMessage("You find a scroll of teleport.")
    PlayerTeleport = PlayerTeleport + 1
    let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
    drawSidebar()
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) = 15
    addMessage("You find a s. of holy light.")
    PlayerHolyLight = PlayerHolyLight + 1
    let DungeonMap(PlayerX+xDir,PlayerY+yDir) = 1
    PlayerX = PlayerX + xDir
    PlayerY = PlayerY + yDir
    drawSidebar()
  elseif DungeonMap(PlayerX+xDir,PlayerY+yDir) < 0
    fightMonster(1,PlayerX+xDir,PlayerY+yDir)
  else
    addMessage("This way is blocked.")
  end if

end sub

sub startScreen()
  ink 7
  bright 1
  print at 0,14;"ZXRL"
  print at 1,12;"-Part I-"
  ink 4
  print at 5,10;"No title yet"
  ink 7
  print at 15,5;"Programmed in 2019 by"
  ink 2
  print at 17,9;"TheMightyGlider"
  ink 7
  print at 23,4; "Press any key to start..."
  waitKey()

end sub

sub deadScreen(deadKind as uByte)
  cls
  ink 7
  bright 1
  if deadKind = 0 then
    print at 0,9;"You are dead!"
    print at 4,0;"But soon your cold flesh will"
    print at 5,0;"move again..."
    print at 10,0;"The necromancer got a new minion"
    print at 11,13;"!!!"
  elseif deadKind = 1
    print at 0,10;"You are insane!"
    print at 4,0;"You start to scream and laugh"
    print at 5,0;"hysterical..."
    print at 10,0;"...until one of the crypt's"
    print at 11,0;"horrors kills you!"
  elseif deadKind = 2
    print at 0,8;"You have made it!"
    print at 4,0;"You killed the evil necromancer!"
    print at 5,0;"You will return as a hero!"
    ink 5
    print at 10,11;"GOOD ENDING"
    ink 7
  elseif deadKind = 3
    print at 0,8;"You have made it!"
    print at 4,0;"You killed the evil necromancer!"
    print at 5,0;"But your journey made you insane"
    print at 6,13;"!!!"
    print at 7,0;"You will take his place now..."
    ink 2
    print at 10,11;"BAD ENDING"
    ink 7
  end if
  print at 23,0;"Press any key to restart..."
  waitKey()

end sub

sub checkDead()
  if PlayerHealth < 1 then
    addMessage("You die!")
    waitKey()
    deadScreen(0)
    runing = 0
  elseif PlayerSanity < 1
    addMessage("You go insane!")
    waitKey()
    deadScreen(1)
    runing = 0
  end if

end sub

dim MainLoop as uByte
MainLoop = 1
do
  setColor()
  cls
  startScreen()
  randomize
  initPlayer()
  cls
  dungeonGenerator()
  setUDC()
  drawSidebar()
  drawMap(0,5,0,5)
  addMessage("Welcome inside the crypt!")
  runing = 1
  do
    moveMonsters(PlayerX-4,PlayerX+4,PlayerY-4,PlayerY+4)
    checkDead()
    if runing = 1 then
      updateExploration()
      drawMap(PlayerX-3,PlayerX+3,PlayerY-3,PlayerY+3)
      userInput()
      checkDead()
    end if
  loop while runing = 1
loop while MainLoop = 1
